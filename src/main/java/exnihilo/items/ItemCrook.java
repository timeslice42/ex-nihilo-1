package exnihilo.items;

import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.world.World;

import com.google.common.collect.Sets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import exnihilo.data.ItemData;
import exnihilo.data.ModData;
import exnihilo.proxies.Proxy;
import exnihilo.utils.CrookUtils;

import net.minecraftforge.common.util.ForgeDirection;

public class ItemCrook extends ItemTool{
	public static final double pullingForce = 1.5d;
	public static final double pushingForce = 1.5d;

	@SuppressWarnings("rawtypes")
	public static final Set blocksEffectiveAgainst = Sets.newHashSet(new Block[]{});

	public ItemCrook() 
	{	
		super(0.0f, ToolMaterial.WOOD, blocksEffectiveAgainst);

		this.setMaxDamage((int)(this.getMaxDamage() * 2));
	}
	
	public ItemCrook(ToolMaterial mat) 
	{	
		super(0.0f, mat, blocksEffectiveAgainst);
	}

	@Override
	public boolean func_150897_b(Block block)
	{	
		if (block.isLeaves(Proxy.getProxy().getWorld(), 0, 0, 0))
		{
			return true;
		}

		return false;
	}

	@Override
	public float func_150893_a(ItemStack item, Block block)
	{	
		if (block.isLeaves(Proxy.getProxy().getWorld(), 0, 0, 0))
		{
			return efficiencyOnProperMaterial + 1;
		}

		return 1.0F;
	}

	//Break leaf block
	@Override
	public boolean onBlockStartBreak(ItemStack item, int X, int Y, int Z, EntityPlayer player)
	{
		CrookUtils.doCrooking(item, X, Y, Z, player);

		return false;
	}

	//Left click entity
	@Override
	public boolean onLeftClickEntity(ItemStack item, EntityPlayer player, Entity entity)
	{
		//TODO: Push entity away from you!
		if (!player.worldObj.isRemote)
		{
			double distance = Math.sqrt(Math.pow(player.posX - entity.posX, 2) + Math.pow(player.posZ - entity.posZ, 2));

			double scalarX = (player.posX - entity.posX) / distance;
			double scalarZ = (player.posZ - entity.posZ) / distance;

			double velX = 0 - scalarX * pushingForce;
			double velZ = 0 - scalarZ * pushingForce;
			double velY = 0; //- (player.posY - entity.posY);

			entity.addVelocity(velX, velY, velZ);
		}
		//Don't do damage
		item.damageItem(1, player);
		return true;
	}

	@Override
	public boolean itemInteractionForEntity(ItemStack item, EntityPlayer player, EntityLivingBase entity)
	{
		double distance = Math.sqrt(Math.pow(player.posX - entity.posX, 2) + Math.pow(player.posZ - entity.posZ, 2));

		double scalarX = (player.posX - entity.posX) / distance;
		double scalarZ = (player.posZ - entity.posZ) / distance;

		double velX = scalarX * pullingForce;
		double velZ = scalarZ * pullingForce;
		double velY = 0; //- (player.posY - entity.posY);

		entity.addVelocity(velX, velY, velZ);

		item.damageItem(1, player);
		return true;
	}
	
	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ)
	{
		if (!world.isRemote && world.getBlock(x, y, z) == Blocks.dirt && world.getBlock(x,  y+1,  z) == Blocks.water && ForgeDirection.getOrientation(side) == ForgeDirection.UP
				&& ModData.LILYPAD_CHANCE > 0)
		{
			if (world.rand.nextInt(ModData.LILYPAD_CHANCE) == 0)
			{
				EntityItem item = new EntityItem(world, x, y+1, z, new ItemStack(Blocks.waterlily));
				world.spawnEntityInWorld(item);
			}
			stack.damageItem(1, player);
			return true;
		}
		return false;
	}

	@Override
	public String getUnlocalizedName()
	{
		return ModData.ID + "." + ItemData.CROOK_UNLOCALIZED_NAME;
	}

	@Override
	public String getUnlocalizedName(ItemStack item)
	{
		return ModData.ID + "." + ItemData.CROOK_UNLOCALIZED_NAME;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register)
	{
		this.itemIcon = register.registerIcon(ModData.TEXTURE_LOCATION + ":Crook");
	}
}