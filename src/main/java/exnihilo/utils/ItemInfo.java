package exnihilo.utils;


import lombok.Data;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Data
public class ItemInfo {

	private Item item;
	private int meta;



	public ItemInfo(ItemStack itemStack)
	{
		if (itemStack != null && itemStack.getItem() != null)
		{
			this.item = itemStack.getItem();
			this.meta = itemStack.getItemDamage();
		}
	}
	
	public ItemInfo(Block block, int meta)
	{
		this.item = Item.getItemFromBlock(block);
		this.meta = meta;
	}
	
	public ItemInfo(Item item, int meta)
	{
		this.item = item;
		this.meta = meta;
	}
	
	public ItemStack getStack()
	{
		return new ItemStack(this.item, 1, this.meta);
	}



}
